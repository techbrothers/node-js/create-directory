var fs = require('fs');

// creating directory and read the file to write into the created directory
fs.mkdir('subFolder', () => {
  fs.readFile('test.txt','utf8',(err, data) => {
    fs.writeFile('./subFolder/newfile.txt',data, () => {
       console.log('data has been wtritten to newfile.txt');
    });
  });
});

// delete file and directory
fs.unlink('./subFolder/newfile.txt', () => {
  fs.rmdir('subFolder', () => {
    console.log('directory has been deleted successfully');
  });
});
